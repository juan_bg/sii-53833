## CHANGELOG

--------------------------

# [v1.8] - 28-12-2020

- Implementada comunicación por sockets

# [v1.7] - 16-12-2020

- Creados el cliente y el servidor
- Añadida comunicación por FIFOS entre cliente y servidor

# [v1.6] - 02-12-2020

- Añadido logger: Muestra la puntuación de cada jugador a tiempo real.
- Añadido bot: Mueve la raqueta 1 en función de la posición de la bola.

# [v1.4] - 29-10-2020

- Movimiento de la esfera y raquetas.
- La esfera cambia de tamaño con el tiempo.
 
# [v1.2] - 15-10-2020

- Creación de los ficheros Changelog y README sintaxis Markdown
 
# [v1.1] - 15-10-2020

- Incluidos comentarios en los ficheros de cabecera
