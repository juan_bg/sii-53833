// MundoCliente.h: interface for the MundoCliente class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#if !defined(AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <vector>
#include "Plano.h"


#if _MSC_VER > 1000
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Socket.h"

//El cliente se comunica con el bot

class MundoCliente  
{
public:
	void Init();
	MundoCliente();
	virtual ~MundoCliente();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera; 
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	
	//int fd2; //descriptor del FIFO con servidor
	//int fd_teclas; //descriptor del FIFO para el envio de las teclas.
	
	Socket s_comunicacion; //Socket para comunicación con servidor.
	
	DatosMemCompartida datosMem;  //Atributo de datos a memoria compartida
	DatosMemCompartida *pdatosMem; //Puntero a datos memoria compartida
	
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
