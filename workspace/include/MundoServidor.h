// MundoServidor.h: interface for the MundoServidor class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <vector>
#include <pthread.h>
#include "Plano.h"


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Socket.h"

//El servidor se comunica con el logger

class MundoServidor  
{
public:
	void Init();
	MundoServidor();
	virtual ~MundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();

	Esfera esfera; 
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	
	int fd; //descriptor del FIFO con logger
	
	//int fd2; //descriptor del FIFO con cliente
	//int fd_teclas; //descriptor del FIFO para teclas
	
	//Sockets para la comunicación con el cliente
	Socket s_comunicacion;
	Socket s_conexion;
	
	
	pthread_t thid1;
	pthread_attr_t atrib;
};

#endif // !defined(AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
