// MundoCliente.cpp: implementation of the MundoCliente class.
//
//Autor: Juan Balaguer García
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////



char cad[200];

MundoCliente::MundoCliente()
{
	Init();
}

MundoCliente::~MundoCliente()
{
	munmap(pdatosMem, sizeof(datosMem));
	unlink("DatosCompartidos");
	
	//close(fd2);
	//unlink("/tmp/fifo_com");
	
	//close(fd_teclas);
	//unlink("/tmp/fifo_teclas");
}


void MundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void MundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void MundoCliente::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	
	//actualización de los datos proyectados en memoria
	pdatosMem-> esfera = esfera;
	pdatosMem -> raqueta1 = jugador1;
	
	//control del bot
	if (pdatosMem -> accion == -1){
	  OnKeyboardDown('s', 0, 0);
	}
	if (pdatosMem -> accion == 1){
	  OnKeyboardDown('w', 0, 0);
	}

	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;	
	}
	
	//lectura de los datos de la tubería
	  //read (fd2, cad, strlen(cad));
	  
	  
	//Recive las coordenadas a través del socket
	s_comunicacion.Receive(cad, sizeof(cad));
	
	//Actualización de los atributos con los datos recibidos
	sscanf(cad, "%f %f %f %f %f %f %f %f %d %d", &esfera.centro.x, &esfera.centro.y, &jugador1.x1,       &jugador1.y1, &jugador1.x2, &jugador1.y2, &jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1, &puntos2); 

}

void MundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char tecla[] = "0";
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's'://jugador1.velocidad.y=-4; 
	         sprintf (tecla, "s");break;
	case 'w'://jugador1.velocidad.y=4;
	         sprintf (tecla, "w");break;
	case 'l'://jugador2.velocidad.y=-4;
	         sprintf (tecla, "l");break;
	case 'o'://jugador2.velocidad.y=4;
	         sprintf (tecla, "o");break;
	}
	
        //write (fd_teclas, &key, sizeof(key));
        
        //Envia las teclas a través del socket
        s_comunicacion.Send(tecla, sizeof(tecla));
}

void MundoCliente::Init()
{
	
	//PROYECCIÓN DE DATOS EN MEMORIA
	char *pfd;
	int fd1 = open("datosCompartidos", O_RDWR | O_CREAT | O_TRUNC, 0666); //creamos el fichero que queremos proyectar
	if (fd1 < 0){
	  perror ("error en el open datos");
	}
	
	datosMem.esfera = esfera;
	datosMem.raqueta1 = jugador1;
	datosMem.accion = 0;
	write (fd1, &datosMem, sizeof(datosMem)); //escribimos los datos en el fichero.
	
	
	pdatosMem = static_cast<DatosMemCompartida*>(mmap (NULL, sizeof(datosMem), PROT_WRITE | PROT_READ, MAP_SHARED, fd1, 0)); //proyección al puntero a DatosMemCompartida
	if (pdatosMem == MAP_FAILED){
	  perror ("error en la proyección de fichero");
	  close(fd1);
	}
	
	close(fd1);
	
	
	
	//CREACIÓN Y APERTURA DEL FIFO CON SERVIDOR
	
		//mkfifo("/tmp/fifo_com", 0666);
		//fd2 = open ("/tmp/fifo_com", O_RDONLY);
	
	//CREACIÓN Y APERTURA DEL FIFO PARA ENVIO DE TECLAS
	
		//mkfifo("/tmp/fifo_teclas", 0666);
		//fd_teclas = open ("/tmp/fifo_teclas", O_WRONLY);
	
	
	//CONEXIÓN CON SERVIDOR
	char ip [] = "127.0.0.1";
	char name[50];
	
	printf("Introduzca un nombre: \n");
	scanf("%s", name);
	s_comunicacion.Connect(ip, 8000);
	
	//Envio del nombre al servidor
	s_comunicacion.Send(name, sizeof(name));
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
