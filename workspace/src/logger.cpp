#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#include "Puntuaciones.h"
#include "glut.h"

//LECTOR DEL FIFO

int main (void)
{       
	int ret;
	int fd;
	Puntuaciones punt; // Vamos a leer de la tubería una estructura de tipo Puntuaciones
	
	ret = mkfifo("mi_fifo", 0666); //creamos el fifo
	//if (ret < 0){
	  //  perror ("error en mkfifo");
	  //  return 1;
	//}
	
	fd = open ("mi_fifo", O_RDONLY); //abrimos en modo lectura
	if (fd < 0){
	   perror ("error en open");
	   return 1;
	}
	
	
	while(1) {
	     if (read (fd, &punt, sizeof(punt)) < sizeof(punt)){
	       perror("error en la lectura");
	       break;
	     }
	       
	     else{
	       if (punt.LastWinner == 1){
	         printf ("El jugador 1 marca 1 punto, lleva un total de %d puntos \n", punt.Pjugador1);
	         if (punt.Pjugador1 == 8){
	           printf("Fin de la partida. Has ganado");
	           close(fd);
		   unlink ("mi_fifo");
		   break;
	         }
	       }
	       if (punt.LastWinner == 2){
	         printf ("El jugador 2 marca 1 punto, lleva un total de %d puntos \n", punt.Pjugador2);
	         if (punt.Pjugador2 == 6){
	           printf("Fin de la partida. Has perdido");
	           close(fd);
		   unlink ("mi_fifo");
		   break;
	         }
	       }
	     }
	}
	
	close(fd);
	unlink ("mi_fifo");
	
	return 0;
	
}

