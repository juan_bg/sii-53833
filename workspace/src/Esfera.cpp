// Esfera.cpp: implementation of the Esfera class.
//
//-Autor: Juan Balaguer García
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	pulso=0.1f;
	velocidad.x=3;
	velocidad.y=3;
	r = 255;
	g = 255;
	b = 0;
	
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(r,g,b);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	//Movimiento sin aceleración
	centro = centro + velocidad*t; //Operadores sobrecargados Vector2D
	
	if (radio > 0.6){
		pulso = -pulso;
	}	
	if (radio < 0.1){
		pulso = -pulso;
	}
	radio = radio + pulso*t;
	b = radio*50;
	g = 255 - radio*100;
}
