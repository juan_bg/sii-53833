// MundoServidor.cpp: implementation of the MundoServidor class.
//
//Autor: Juan Balaguer García
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <math.h>
#include <sys/mman.h>
#include "Puntuaciones.h"



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//El servidor se va a comunicar con el logger

char cad[200];

MundoServidor::MundoServidor()
{
	Init();
}

MundoServidor::~MundoServidor()
{
	close(fd); //cerramos la tubería con logger
	unlink("mi_fifo");
	
	//close(fd2); //cerramos la tubería con cliente
	//unlink("/tmp/fifo_com");
	
	//close(fd_teclas); //cerramos la tubería para el paso de teclas
	//unlink("/tmp/fifo_teclas");
}


void MundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}


void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void MundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void *hilo_comandos(void *d)
{
	MundoServidor *p = (MundoServidor*) d;
	p -> RecibeComandosJugador();
}

void MundoServidor::RecibeComandosJugador()
{
	//Apertura del FIFO con cliente para las teclas en modo lectura
		//fd_teclas = open("/tmp/fifo_teclas", O_RDONLY);
	
	while(1){
		usleep(10);
		char cad[100];
		unsigned char key;
		//read(fd_teclas, cad, sizeof(cad));
		s_comunicacion.Receive(cad, sizeof(cad)); //recibo la cadena por sockets
		sscanf(cad, "%c", &key);
		if (key == 's') jugador1.velocidad.y = -4;
		if (key == 'w') jugador1.velocidad.y =  4;
		if (key == 'l') jugador2.velocidad.y = -4;
		if (key == 'o') jugador2.velocidad.y =  4;
	}
}
 

void MundoServidor::OnTimer(int value)
{	
	Puntuaciones punt;
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);


	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		punt.LastWinner = 2;
		punt.Pjugador1 = puntos1;
		punt.Pjugador2 = puntos2;		
      		
      		//escritura del mensaje en el FIFO
		write(fd, &punt, sizeof(punt));
		
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		punt.LastWinner = 1;
		punt.Pjugador1 = puntos1;
		punt.Pjugador2 = puntos2;		
      		
      		//escritura del mensaje en el FIFO logger
		write(fd, &punt, sizeof(punt));
	}
	
	//escribimos los datos para pasarselos al cliente
	sprintf(cad, "%f %f %f %f %f %f %f %f %d %d", esfera.centro.x, esfera.centro.y, jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2, jugador2.x1, jugador2.y1, jugador2.x2, jugador2.y2, puntos1, puntos2); 
	
	//escribimos los datos en la tubería que comunica con cliente
	//write(fd2, &cad, strlen(cad));
	
	//envio de las coordenadas a través del socket al cliente
	s_comunicacion.Send(cad,sizeof(cad)); 
	

}

void MundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void MundoServidor::Init()
{

	//abrimos la tubería con logger en modo escritura
	fd = open ("mi_fifo", O_WRONLY);
	//abrimos la tubería con cliente en modo escritura	
		//fd2 = open ("/tmp/fifo_com", O_WRONLY);



	//CONEXIÓN DEL SOCKET
	char ip [] = "127.0.0.1";
	if (s_conexion.InitServer(ip,8000) == -1)
		printf("error abriendo el servidor\n");
	s_comunicacion = s_conexion.Accept();
		//Recibimos el nombre del cliente
	char name [50];
	s_comunicacion.Receive(name, sizeof(name));
	printf("%s se ha conectado a la partida.\n", name);
	
	
	
	//Creación del thread
	
	pthread_attr_init(&atrib);
	pthread_attr_setdetachstate(&atrib, PTHREAD_CREATE_JOINABLE);
	
	pthread_create(&thid1, &atrib, hilo_comandos, this);
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
